If you've come here looking for projects of Benjamin Dobell (Github user
[Benjamin-Dobell](https://github.com/Benjamin-Dobell)), my GitLab username is
[BenjaminDobell](https://gitlab.com/BenjaminDobell) i.e. no hyphen.